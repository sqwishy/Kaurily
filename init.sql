drop table if exists sub;
drop table if exists msg;

-- Messages

create table msg (
    id          uuid not null
                default uuid_generate_v4(),
    --identity    bytea
    --            check (length(identity) < 256),
    topic       bytea
                check (length(topic) < 256),
    data        bytea
                check (length(data) < 4096),
    --created_at  timestamp with time zone default now() not null,

    constraint msg_pkey primary key (id)
);

--create index msg_topic_idx
--    on msg
--    using btree (topic);

-- Subscriptions

create table sub (
    id              uuid not null
                    default uuid_generate_v4(),
    identity        bytea not null
                    check (length(identity) < 256),
    topic           bytea not null
                    check (length(topic) < 256),
    cursor          uuid
                    references msg(id),
    -- We don't really care about this column, but I think I need it for `on
    -- conflict` ... TODO ...
    last_subscribed timestamp with time zone default now() not null,
    constraint sub_pkey primary key (id)
);

create unique index sub_identity_topic_idx
    on sub (identity, topic);

-- Notify after insert on msg

drop function if exists notify_subs();

create function notify_subs() returns trigger as $$
begin
    perform pg_notify('_wow', id::text)
        from sub where topic=new.topic;
    perform pg_notify(id::text, new.id::text)
        from sub where topic=new.topic;
    return new;
end
$$ language plpgsql;

create trigger do_notify_subs
    after insert on msg
    for each row
    execute procedure notify_subs();

-- Listen after insert/update on sub

drop function if exists listen_to_sub();

create function listen_to_sub() returns trigger as $$
begin
    execute format('listen "%s"', new.id::text);
    return new;
end
$$ language plpgsql;

create trigger do_listen_to_sub_insert
    after insert on sub
    for each row
    execute procedure listen_to_sub();

create trigger do_listen_to_sub_update
    after update on sub
    for each row
    execute procedure listen_to_sub();
