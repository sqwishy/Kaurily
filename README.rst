Kaurily
=======

A message queue thing built in Python on top of PostgreSQL.

This project is not stable or supported. It also probably doesn't work great.
Don't use it for anything.

Design
------

::

    => Syncronous       verb()  A callable invocation, may be via a Task
    -> Asyncronous              or asynchronously if a coroutine.
                        [batch] A place we syncronously store information for
                                later async batch execution.

    For each connection ...
    +-----+  +-ServerProtocol--+ *    1 +-Voice------+
    | TCP |=>| data_received() |=======>| announce() |=>[batch]-+
    +-----+  +-----------------+   |    +------------+          |
             | Protbuf         |   |  1 +-Ears-----+            |
             +-----------------+   +===>| listen() |=>[batch]   |
                       ^                +----------+   |        |
                       |                               |   (Voice task)
                       |                               |        |
                       +=(Ears notify task)  (Ears listen task) |
                                 ^       |             |        |
                    +=NOTIFY=>[batch]    |             |        |
                    |                    |             |        |
    +------------+ +------------+        |             |        |
    |            | | Connection |-SELECT-+             |        |
    +            + +------------+                      |        |
    | PostgreSQL | | Connection |-INSERT---------------+        |
    |            | +------------+                               |
    |            | | Connection |-INSERT------------------------+
    +------------+ +------------+                                

Ideally, the order of importance:
  - Move data from PostgreSQL notifications to sockets.
  - Handle requests that have accumulated in batches for quite some time.
  - Read requests from sockets and schedule place them into batches.

System Setup
------------

The database is PostgreSQL. It was developed against 9.6.9. I don't know what
the minimum version required is, but it's probably 9.5 because of the ON
CONFLICT clause for INSERT.

.. code-block:: 

    $ createdb -Ttemplate1 kaurily
    $ psql kaurily -f init.sql

Tests
-----

They try to connect to PostgreSQL and use a database called kaurily.
