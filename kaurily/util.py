import asyncio
import contextlib
import logging
import time
import traceback

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def record_duration(name, logger=logger, **kwargs):
    above, *_ = traceback.extract_stack(limit=3)
    start = time.monotonic()
    yield
    duration = time.monotonic() - start
    logger.debug(
        "Measured [%s/%s] %s %0.2f ms",
        above.name,
        name,
        ",".join("%s=%r" % i for i in kwargs.items()),
        duration * 1000,
    )


async def gather(*fs, loop=None):
    """ Yikes! """
    if not fs:
        raise ValueError("No futures given...")

    done, pending = set(), set(asyncio.ensure_future(f, loop=loop) for f in fs)
    try:
        done, pending = await asyncio.wait(
            pending, loop=loop, return_when=asyncio.FIRST_EXCEPTION
        )
    except asyncio.CancelledError:
        pass

    if pending:
        for task in pending:
            task.cancel()
        pending_done, still_pending = await asyncio.wait(pending, loop=loop)
        assert not still_pending

    for task in done:
        task.result()

    if pending:
        for task in pending_done:
            task.result()
