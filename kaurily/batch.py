import asyncio
import logging
import time
from datetime import timedelta
from typing import Any, Coroutine, List, Sequence

import attr  # type: ignore

logger = logging.getLogger(__name__)


class BatchFull(Exception):
    pass


@attr.s()
class BatchWaiter(object):
    future: asyncio.Future = attr.ib()
    something: Any = attr.ib()


@attr.s()
class Batch(object):
    """
    Allows syncronous insertion. Eventually, the `asyncio.Event` `take_me` is
    set to let a coroutine (which you are responsible for) know it should take
    the contents batch, emptying it.
    """

    delay: float = attr.ib(
        default=timedelta(milliseconds=10), convert=timedelta.total_seconds
    )
    # delay_max: float = attr.ib(
    #    default=timedelta(milliseconds=100)
    # )
    capacity: int = attr.ib(default=None)
    loop = attr.ib(default=attr.Factory(asyncio.get_event_loop))

    take_me: asyncio.Event = attr.ib(
        default=attr.Factory(lambda s: asyncio.Event(loop=s.loop), takes_self=True)
    )

    _pending: List[BatchWaiter] = attr.ib(default=attr.Factory(list))
    _pending_since = attr.ib(default=None)
    _take_me_timer = attr.ib(default=None)

    # @delay_max.validator
    # def max_ge_min(self, attribute, value):
    #    if value < self.delay_min:
    #        raise ValueError(
    #            "delay_max lower than delay_min: %s < %s" % (value, self.delay_min)
    #        )

    @classmethod
    def attr_factory(cls, *args, **kwargs):
        def make_batch_for(owner):
            if hasattr(owner, "loop"):
                kwargs["loop"] = owner.loop
            return cls(*args, **kwargs)

        return attr.Factory(make_batch_for, takes_self=True)

    def is_at_capacity(self):
        return self.capacity is not None and len(self._pending) >= self.capacity

    def put_nowait(self, *something: Any) -> asyncio.Future:
        """
        Tries to add something to the batch, returning a future when it has
        been processed.
        
        If capacity is set, it will raise BatchFull.
        """
        f: asyncio.Future = asyncio.Future(loop=self.loop)
        self._put(BatchWaiter(f, something))
        return f

    def put_nofuture(self, *something: Any):
        self._put(BatchWaiter(None, something))

    def _put(self, waiter):
        if self.is_at_capacity():
            raise BatchFull(self.capacity)

        if self._pending_since is None:
            # First insert since _pending was cleared
            self._pending_since = time.monotonic()
            self._take_me_timer = self.loop.call_later(self.delay, self.take_me.set)

        self._pending.append(waiter)

        if self.is_at_capacity():
            self._take_me_timer.cancel()
            self.take_me.set()

    def take_all(self):
        pending = self._pending
        self._pending = []
        self._pending_since = None
        self.take_me.clear()
        return pending

    async def consume_forever(self, *args, **kwargs):
        """ Runs consume_once() in a loop until we're canceled.
        """
        while True:
            try:
                await self.consume_once(*args, **kwargs)
            except asyncio.CancelledError:
                break

    async def consume_once(self, handler: Any):
        """ Takes a ...
        """
        await self.take_me.wait()
        waiters = self.take_all()
        if not waiters:
            logger.warning(
                "No waiters returned from take_all() from a batch for %r?", handler
            )
        else:
            try:
                results = await handler(tuple(waiter.something for waiter in waiters))
                if results:  # Probably the waiters were created without futures ...
                    for waiter, result in zip(waiters, results):
                        waiter.future.set_result(result[0])
            except Exception as ex:
                for waiter in waiters:
                    if not waiter.future.done():
                        waiter.future.set_exception(ex)
                raise
