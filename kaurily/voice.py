"""
Then the Lord spoke to you out of the fire. You heard the sound of words
but saw no form; there was only a voice.
"""
import asyncio
from datetime import timedelta
from itertools import chain

import attr  # type: ignore

from kaurily.batch import Batch
from kaurily.util import record_duration


@attr.s()
class Voice(object):
    """ Allows publishing announcements to subscribers on the network.

    You need to await this, to create a task that does the stuff that makes
    this work.
    """

    pg = attr.ib()
    loop = attr.ib(default=attr.Factory(asyncio.get_event_loop))
    _batch = attr.ib(
        default=Batch.attr_factory(capacity=15_000, delay=timedelta(milliseconds=20))
    )

    def __await__(self):
        return self._batch.consume_forever(self._insert_messages).__await__()

    def announce(self, topic: bytes, payload: bytes) -> asyncio.Future:
        return self._batch.put_nowait(topic, payload)

    async def _insert_messages(self, messages):
        # P.S.: number of query args can't excede like 32k or something ...
        query_string = (
            """
            insert into msg (topic, data) values
            """
            + ",".join(
                "($%d, $%d)" % (i + 1, i + 2) for i in range(0, len(messages) * 2, 2)
            )
            + """
            returning id::text
            """
        )
        values = chain.from_iterable(messages)
        with record_duration("pg.fetch", messages=len(messages)):
            msgs = await self.pg.fetch(query_string, *values)
        return msgs
