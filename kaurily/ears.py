"""
True hearts have eyes and ears, no tongues to speak;
They hear and see, and sigh, and then they break. 
"""
import asyncio
from datetime import timedelta
from itertools import chain

import attr  # type: ignore

from kaurily.batch import Batch
from kaurily.kaurily_pb2 import Announcement, Message
from kaurily.pgsql import Connection
from kaurily.util import gather, record_duration


@attr.s()
class Perspective(object):
    identity: bytes = attr.ib()
    topic: bytes = attr.ib()


@attr.s()
class Ears(object):
    """
    Allows creating subscriptions to listen for announcements.
    And does the work of handling announcement notifications from PostgreSQL
    and routing them to network buffers of subscribers.

    You need to await this, to create a task that does the stuff that makes
    this work.
    """

    pg_listen: Connection = attr.ib()
    pg_propogate: Connection = attr.ib()
    loop = attr.ib(default=attr.Factory(asyncio.get_event_loop))
    _batch_listen = attr.ib(
        default=Batch.attr_factory(capacity=15_000, delay=timedelta(milliseconds=20))
    )
    _batch_propogate = attr.ib(
        default=Batch.attr_factory(delay=timedelta(milliseconds=20))
    )
    # Used to connect listeners/subscribers with incoming
    # notifies/announcements from PostgreSQL.
    _subscription_queues = attr.ib(default=attr.Factory(dict))

    def __attrs_post_init__(self):
        assert self.pg_listen._process_notification is NotImplemented

        def queue_notification(pid, channel, payload):
            self._batch_propogate.put_nofuture(channel, payload)

        self.pg_listen._process_notification = queue_notification

    def __await__(self):
        listen = self._batch_listen.consume_forever(self._insert_subscriptions)
        propogate = self._batch_propogate.consume_forever(self._propogate_notifications)
        return gather(listen, propogate, loop=self.loop).__await__()

    def listen(self, perspective: Perspective, queue: asyncio.Queue) -> asyncio.Future:
        """
        Schedules a subscription to take place.

        If you listen to the same thing twice, it will happily do that, but
        you will only get one notification ... 🤔
        """
        return self._batch_listen.put_nowait(perspective, queue)

    async def _insert_subscriptions(self, listens):
        # P.S.: number of query args can't excede like 32k or something ...
        query_string = (
            """
            insert into sub (identity, topic) values
            """
            + ",".join(
                "($%d, $%d)" % (i + 1, i + 2) for i in range(0, len(listens) * 2, 2)
            )
            + """
            on conflict (identity, topic) do update set last_subscribed=now()
            returning id::text
            """
        )
        # TODO handle/ignore/prevent duplicate values ...
        values = chain.from_iterable(
            (perspective.identity, perspective.topic) for perspective, queue in listens
        )
        with record_duration("pg.fetch", subscriptions=len(listens)):
            subs = await self.pg_listen.fetch(query_string, *values)
        for (sub_id,), (_, queue) in zip(subs, listens):
            self._subscription_queues.setdefault(sub_id, set()).add(queue)
        return subs

    async def _propogate_notifications(self, notifications):
        """
        Handle a bunch of notifications, which are channel-payload pairs.
        The channel is the subscription id, the payload is the msg id.
        """
        with record_duration("pg.fetch", notifications=len(notifications)):
            res = await self.pg_propogate.fetch(
                "select id, topic, data from msg where id = any($1::uuid[])",
                set(msg_id for (_, msg_id) in notifications),
            )

        msg_announcements = {
            str(id): Announcement(topic=topic, payload=payload)
            for (id, topic, payload) in res
        }

        # Tell all clients listening to sub_id about the payload we looekd up
        for sub_id, msg_id in notifications:
            clients = self._subscription_queues.get(sub_id)
            if not clients:
                continue
            # We have clients to send to about this subscription...
            msg = Message(announcement=msg_announcements[msg_id])
            for client in clients:
                client.send_message(msg)
        return []
