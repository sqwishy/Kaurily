import asyncpg


class Connection(asyncpg.Connection):
    """
    This guy is almost pointless, we monkey patch the _process_notification
    method from the base class, it's mainly for making sure that we don't make
    a mistake; like call reset() somehow.
    """

    _process_notification = NotImplemented

    async def reset(self, *, timeout=None):
        raise Exception("No, we don't want to do this ...")

    def _get_reset_query(self):
        raise Exception("Please no!")

    async def add_listener(self, channel, callback):
        raise NotImplementedError("Don't call this")

    async def remove_listener(self, channel, callback):
        raise NotImplementedError("Don't call this")


def connect(dburl, loop=None):
    return asyncpg.connect(database=dburl, connection_class=Connection, loop=loop)
