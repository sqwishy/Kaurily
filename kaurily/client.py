import asyncio
import logging
import typing

import attr  # type: ignore

from kaurily.kaurily_pb2 import (Announcement, Error, Listen, Message, Ok,
                                 Pilot, Ping, Pong, Reply, Request, Unlisten)
from kaurily.protocol import ClientProtocol

logger = logging.getLogger(__name__)


@attr.s()
class Client(object):
    """ Wrapper around a the kaurily ClientProtocol providing a nicer interface
    """

    protocol: ClientProtocol = attr.ib()

    def listen(self, topic: bytes, identity: bytes) -> asyncio.Future:
        return self.protocol.send_request(
            Request(listen=Listen(topic=topic, identity=identity))
        )

    def announce(self, topic: bytes, payload: bytes) -> asyncio.Future:
        return self.protocol.send_request(
            Request(announcement=Announcement(topic=topic, payload=payload))
        )

    def ping(self) -> asyncio.Future:
        return self.protocol.send_request(Request(ping=Ping()))
