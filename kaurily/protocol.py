"""
Provides implementation of the protocol used to talk with a Kaurily over a byte
stream.

Messages are encoded with Protocol Buffer version 3.

[ Pilot ]
"""
import asyncio
import itertools
import logging
from typing import Union

from kaurily.ears import Ears, Perspective
from kaurily.kaurily_pb2 import (Announcement, Error, Listen, Message, Ok,
                                 Pilot, Ping, Pong, Reply, Request, Unlisten)
from kaurily.util import record_duration
from kaurily.voice import Voice

logger = logging.getLogger(__name__)


REQUEST_ID_MAX = 2 ** 64
PILOT_SIZE = Pilot(len=1).ByteSize()
assert PILOT_SIZE == 5

MessageTypes = Union[Request, Reply, Announcement, Error]
RequestTypes = Union[Ping, Listen, Unlisten, Announcement]
ReplyTypes = Union[Pong, Ok, Error]


def request_id_counter():
    return itertools.cycle(range(REQUEST_ID_MAX))


def pb_unwrap(obj, attr="kind"):
    oneof = obj.WhichOneof(attr)
    if oneof is None:
        raise ValueError(
            "Malformed message? `%s` is not a %s of anything." % (obj, attr)
        )
    return getattr(obj, oneof)


def transport_peername(transport):
    peername = transport.get_extra_info("peername")
    if isinstance(peername, tuple):
        try:
            addr, port = transport.get_extra_info("peername")
        except ValueError:
            return "???"
        else:
            return "{}:{}".format(addr, port)
    else:
        return peername


class RemoteException(Exception):
    pass


class BaseProtocol(asyncio.Protocol):
    """ 
    Reads and writes messages ...
    """

    def __init__(self, loop=None):
        super().__init__()
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop
        self.peername = None  # Set in connection_made()
        self.transport = None  # Set in connection_made()
        self.logger = logger
        self.buf = bytearray()

    def connection_made(self, transport):
        self.peername = transport_peername(transport)
        logger.info("Connection made with %s", self.peername)
        self.transport = transport
        self.logger = logger.getChild(self.peername)

    def connection_lost(self, exc):
        self.logger.info("Connection lost to %s: %s", self.peername, exc)
        # TODO self.ears.forget_client(self)

    def data_received(self, data):
        self.buf.extend(data)
        while True:
            view = memoryview(self.buf)
            if len(view) >= PILOT_SIZE:
                p = Pilot()
                try:
                    p.MergeFromString(bytes(view[:PILOT_SIZE]))
                except:
                    logger.exception(
                        "Something terrible happened reading from the transport."
                    )
                    self.transport.close()
                    break
                msg_len = p.len
            else:
                break  # No complete pilot, stop
            msg_end = PILOT_SIZE + msg_len
            if len(self.buf) >= msg_end:
                # We have a message!
                msg_buf = bytes(view[PILOT_SIZE:msg_end])
                del view  # release() ..
                self.buf = self.buf[msg_end:]
                self._deserialize_message(msg_buf)
            else:
                break  # No complete message, stop

    def _deserialize_message(self, data):
        m = Message()
        try:
            m.MergeFromString(data)
            m = pb_unwrap(m)
            self.receive_message(m)
        # except NotImplementedError: TODO consider handling this differently
        except Exception as exc:
            self.logger.exception("Bad or unsupported message %r", data)
            # Something seems wrong with them or us, reply with an error and
            # terminate the connection ...
            try:
                self.send_error_message("I couldn't handle something you sent me.")
            finally:
                self.transport.close()

    def receive_message(self, m: MessageTypes):
        """
        Called when a message has been read from the byte stream.

        Reimplement this in a subclass. The base implementation raises
        NotImplementedError, which you should probably call if you don't
        handle the message.
        """
        raise NotImplementedError(
            "This protocol doesn't support the message you sent it."
        )

    def send_error_message(self, message: str):
        """
        When something horrible happens about no request in particular, you can
        send an error message with this. Otherwise, use send_error_reply to
        reply to a request with an error.
        """
        msg = Message(error=Error(message=message))
        self.send_message(msg)

    def send_message(self, msg: Message):
        """
        Serializes and writes a message, also writes pilot message ahead of it
        as a delimiter. Use this instead of writing to the transport directly.

        Will raise an exception if your message is larger than 4 GB. But if the
        program even gets to that point it would be a miracle.
        """
        msg_bytes = msg.SerializeToString()
        try:
            pilot_bytes = Pilot(len=len(msg_bytes)).SerializeToString()
        except ValueError:
            raise ValueError("Message too large ...")
        self.transport.write(pilot_bytes)
        self.transport.write(msg_bytes)


class ClientProtocol(BaseProtocol):
    """
    For sending Requests and receiving Replies and Announcements.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.requests_in_flight = {}
        self.gen_req_id = request_id_counter()
        self.announcements = asyncio.Queue(loop=self.loop)

    def receive_message(self, m: MessageTypes):
        if isinstance(m, Announcement):
            self.announcements.put_nowait(m)

        elif isinstance(m, Reply):
            self.receive_reply(m.id, pb_unwrap(m))

        else:
            super().receive_message(m)

    def receive_reply(self, id, rep: ReplyTypes):
        if isinstance(rep, (Ok, Error)):
            try:
                f = self.requests_in_flight[id]
            except KeyError:
                self.logger.error(
                    "Received a response for a request that I couldn't find... %r", rep
                )
            else:
                if isinstance(rep, Error):
                    f.set_exception(RemoteException(rep.message))
                else:
                    f.set_result(rep)

        elif isinstance(rep, Pong):
            logger.info("Ponged!")

        else:
            raise NotImplementedError(rep)

    def send_request(self, request) -> asyncio.Future:
        """ Write a request, returning an awaitable future with the response ...
        """
        if not request.id:
            request.id = next(self.gen_req_id)
        elif request.id in self.requests_in_flight:
            raise ValueError("Request with id %r already sent?" % (request.id,))

        self.send_message(Message(request=request))

        f: asyncio.Future = asyncio.Future(loop=self.loop)
        self.requests_in_flight[request.id] = f
        return f


class ServerProtocol(BaseProtocol):
    """
    For receiving Requests and sending Replies and Announcements.
    """

    def __init__(self, ears, voice, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ears = ears
        self.voice = voice

    def send_error_reply(self, id: int, message: str):
        msg = Message(reply=Reply(id=id, error=Error(message=message)))
        self.send_message(msg)

    def receive_message(self, m: MessageTypes):
        if isinstance(m, Request):
            try:
                self.receive_request(m)
            except Exception as exc:
                self.send_error_reply(m.id, str(exc))
        else:
            super().receive_request(m)

    def receive_request(self, req: RequestTypes):
        req_id = req.id
        req = pb_unwrap(req)

        if isinstance(req, Ping):
            reply = Reply(id=req_id, pong=Pong())
            self.send_reply(reply)

        elif isinstance(req, Listen):
            perspective = Perspective(req.identity, req.topic)
            f = self.ears.listen(perspective, self)

            @f.add_done_callback
            def listen_done(f):
                try:
                    f.result()
                except Exception as exc:
                    self.send_error_reply(req_id, str(exc))
                else:
                    self.send_reply(Reply(id=req_id, ok=Ok()))

        elif isinstance(req, Unlisten):
            raise NotImplementedError(req)

        elif isinstance(req, Announcement):
            f = self.voice.announce(req.topic, req.payload)

            @f.add_done_callback
            def announce_done(f):
                try:
                    f.result()
                except Exception as exc:
                    self.send_error_reply(req_id, str(exc))
                else:
                    self.send_reply(Reply(id=req_id, ok=Ok()))

        else:
            raise NotImplementedError(req)

    def send_reply(self, reply: Reply):
        try:
            msg = Message(reply=reply)
            self.send_message(msg)
            # self.logger.debug("Wrote reply %s", reply)
        except Exception as exc:
            try:
                self.send_error_reply(reply.id, str(exc))
            except:
                # Log the send_error_reply() exception, continune and raise the
                # original exception
                self.logger.exception(
                    "Failed to send a reply %r, then failed to send an error instead.",
                    reply,
                )
            else:
                # We sent an error reply, log the exception and do not raise
                self.logger.exception(
                    "Failed to send a reply %r, sent an error instead.", reply
                )
                return
            raise
