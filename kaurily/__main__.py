"""
ʘ‿ʘ
"""
import asyncio
import logging
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser

import uvloop

from kaurily import Ears, ServerProtocol, Voice, connect
from kaurily.util import gather

logger = logging.getLogger("kaurily")


def main():
    parser = ArgumentParser(
        prog=__package__,
        description=__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--listen",
        default=AddressPort.from_string("127.0.0.1:8901"),
        type=AddressPort.from_string,
        help="Address to listen on.",
    )
    parser.add_argument(
        "--dburl", default="kaurily", help="Database thing to connect to."
    )
    parser.add_argument(
        "-l", "--log-level", choices=["DEBUG", "INFO", "WARNING"], default="INFO"
    )
    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level))
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    def pg():
        return loop.run_until_complete(connect(args.dburl, loop=loop))

    loop = asyncio.get_event_loop()
    ears = Ears(pg(), pg())
    voice = Voice(pg())

    def proto_fact():  # Each client connection will create a new instance of ServerProtocol
        return ServerProtocol(ears, voice, loop=loop)

    server = loop.run_until_complete(loop.create_server(proto_fact, *args.listen))
    ears_task = asyncio.ensure_future(ears)
    voice_task = asyncio.ensure_future(voice)

    logging.info("Serving on %s", server.sockets[0].getsockname())
    try:
        loop.run_until_complete(gather(ears_task, voice_task))
    except KeyboardInterrupt:
        pass
    except (SystemExit, KeyboardInterrupt):
        raise
    finally:
        logging.info("Shutting down 🤔")
        voice_task.cancel()
        ears_task.cancel()
        server.close()
        loop.run_until_complete(ears_task)
        loop.run_until_complete(voice_task)
        loop.run_until_complete(server.wait_closed())
        loop.close()


class AddressPort(tuple):
    @classmethod
    def from_string(cls, text):
        # def __new__(cls, text):
        address, port = text.rsplit(":", maxsplit=1)
        return cls((address, int(port)))

    def __str__(self):
        return "%s:%d" % self


if __name__ == "__main__":
    main()
