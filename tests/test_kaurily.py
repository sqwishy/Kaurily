import asyncio
import time

import asyncpg  # type: ignore
import pytest  # type: ignore

from kaurily import (Client, ClientProtocol, Ears, RemoteException,
                     ServerProtocol, Voice, connect)

DATABASE = "kaurily"


@pytest.fixture()
def truncate_db(event_loop):
    conn = event_loop.run_until_complete(connect(DATABASE, loop=event_loop))
    event_loop.run_until_complete(conn.execute("truncate sub, msg cascade"))


@pytest.fixture()
def make_pg(event_loop, truncate_db):
    return lambda: event_loop.run_until_complete(connect(DATABASE, loop=event_loop))


@pytest.fixture
def ears(event_loop, make_pg):
    return Ears(make_pg(), make_pg(), loop=event_loop)


@pytest.fixture
def voice(event_loop, make_pg):
    return Voice(make_pg(), loop=event_loop)


@pytest.fixture
def server_client(ears, voice):
    loop = ears.loop
    addr = "/tmp/kaurily-test.sock"

    def server_protocol():
        return ServerProtocol(ears, voice, loop=loop)

    def client_protocol():
        return ClientProtocol(loop=loop)

    server_proto = loop.run_until_complete(
        loop.create_unix_server(server_protocol, addr)
    )
    transport, client_proto = loop.run_until_complete(
        loop.create_unix_connection(client_protocol, addr)
    )

    ears_task = asyncio.ensure_future(ears)
    # ears_task.add_done_callback(lambda f: loop.stop())
    voice_task = asyncio.ensure_future(voice)
    # voice_task.add_done_callback(lambda f: loop.stop())

    try:
        yield server_proto, Client(client_proto)
    finally:
        voice_task.cancel()
        ears_task.cancel()
        transport.close()
        server_proto.close()

        loop.run_until_complete(ears_task)
        loop.run_until_complete(voice_task)
        loop.run_until_complete(server_proto.wait_closed())
        loop.run_until_complete(asyncio.sleep(0.01))


@pytest.mark.asyncio
async def test_bench(event_loop, server_client):
    server, client = server_client

    sub = await client.listen(topic=b"interesting things", identity=b"client")


@pytest.mark.asyncio
async def test_Everything(event_loop, server_client):
    server, client = server_client
    complete, pending = await asyncio.wait(
        [
            client.listen(
                topic=b"the price of rice in china", identity=b"client %d" % i
            )
            for i in range(1_000)
        ],
        loop=event_loop,
        timeout=1,
    )
    assert not pending
    for f in complete:
        assert f.result()

    await asyncio.wait_for(
        client.announce(topic=b"the price of rice in china", payload=b"potato dollars"),
        timeout=5,
    )

    for _ in range(1_000):
        r = await asyncio.wait_for(client.protocol.announcements.get(), timeout=.1)
        assert r.topic == b"the price of rice in china"
        assert r.payload == b"potato dollars"
