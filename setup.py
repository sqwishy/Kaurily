from codecs import open
from os import path

from setuptools import find_packages, setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="kaurily",
    version="0.1.0",
    description="A message queue thing built in Python on top of PostgreSQL.",
    long_description=long_description,
    url="https://gitlab.com/sqwishy/impetuous",
    #
    author="sqwishy",
    author_email="somebody@froghat.ca",
    license="",
    #
    packages=find_packages(),
    install_requires=[
        "asyncpg>=0.16.0",
        "attrs>=18.1.0",
        "protobuf>=3.6",
        "uvloop>=0.11.0",
    ],
    entry_points={"console_scripts": ["kaurily=kaurily.__main__:main"]},
    tests_require=["pytest", "pytest-asyncio", "pytest-benchmark"],
)
